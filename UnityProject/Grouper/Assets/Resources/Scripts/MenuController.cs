﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Button playButton;

	private void OnEnable ()
    {
        playButton.onClick.AddListener(Play);
	}

    private void OnDisable()
    {
        playButton.onClick.RemoveListener(Play);
    }

    private void Play()
    {
        SceneManager.LoadScene("GrouperMain");
    }
}
