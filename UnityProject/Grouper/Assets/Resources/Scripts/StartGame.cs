﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{

	public void StartOldButton()
    {
        SceneManager.LoadScene("GrouperMain");
    }
    public void StartPCGButton()
    {
        SceneManager.LoadScene("GrouperMainRandomNewNavMesh");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
