﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreenController : MonoBehaviour
{
    private void OnEnable()
    {
        Maze.OnMazeFinishedGenerating += RemoveLoadingScreen;
    }

    private void OnDisable()
    {
        Maze.OnMazeFinishedGenerating -= RemoveLoadingScreen;
    }

    private void RemoveLoadingScreen()
    {
        gameObject.SetActive(false);
    }
}
