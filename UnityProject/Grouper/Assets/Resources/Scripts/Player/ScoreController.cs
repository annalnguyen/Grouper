﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    private int scoreRed;
    private int scoreBlue;

    [SerializeField] private Text scoreTextRed;
    [SerializeField] private Text scoreTextBlue;
    [SerializeField] private Text WinText;
    [SerializeField] private Text fuzzyStory;
    [SerializeField] private GameObject WinCanvas;

    // Use this for initialization
    private void Start ()
    {
        scoreRed = 0;
        scoreBlue = 0;
        scoreTextRed.text = "Red Score:\n" + scoreRed.ToString();
        scoreTextBlue.text = "Blue Score:\n" + scoreBlue.ToString();
        WinText.text = "Winner";
        WinCanvas.SetActive(false);
    }

    private void OnEnable()
    {
        FishyController.OnGoalEnteredFishy += UpdateScore;
    }

    private void OnDisable()
    {
        FishyController.OnGoalEnteredFishy -= UpdateScore;
    }

    private void UpdateScore(Team t)
    {
        if (t == Team.red)
        {
            scoreRed++;
            scoreTextRed.text = "Red Score:\n" + scoreRed.ToString();
        }
        else if (t == Team.blue)
        {
            scoreBlue++;
            scoreTextBlue.text = "Blue Score:\n" + scoreBlue.ToString();
        }
    }

    public void Winner()
    {
        WinCanvas.SetActive(true);
        if (scoreRed > scoreBlue)
        {
            WinText.text = "Red Win's!";
            WinText.color = new Color(255, 0, 0);
            if (scoreRed < 30)
            {
                fuzzyStory.text = "Red tribe leader, even though you have sucessfully recruited the most youths even under the attack of the fisherman and his crafty tricks, you did not recruit enough to gain a promotion. You suck.";
            }
            else if ( scoreRed < 50)
            {
                fuzzyStory.text = "Red tribe leader, even though you have successfully recruited the most youths even under the attack of the fisherman and his crafty tricks, you didn't do well enough to make me like you. Keep trying bud.";
            }
            else if (scoreRed < 80)
            {
                fuzzyStory.text = "Red tribe leader, you collected so many fish! I am so proud of you! Keep working hard!";
            }
            else
            {
                fuzzyStory.text = "YOU ARE THE RED FISH AMONG FISHES! PRAISE THE RED FISH ALMIGHTY";
            }
        }
        else if (scoreBlue > scoreRed)
        {
            WinText.text = "Blue Win's!";
            WinText.color = new Color(0, 0, 255);
            if (scoreBlue < 30)
            {
                fuzzyStory.text = "Blue tribe leader, even though you have sucessfully recruited the most youths even under the attack of the fisherman and his crafty tricks, you did not recruit enough to gain a promotion. You suck.";
            }
            else if (scoreBlue < 50)
            {
                fuzzyStory.text = "Blue tribe leader, even though you have successfully recruited the most youths even under the attack of the fisherman and his crafty tricks, you didn't do well enough to make me like you. Keep trying bud.";
            }
            else if (scoreBlue < 80)
            {
                fuzzyStory.text = "Blue tribe leader, you collected so many fish! I am so proud of you! Keep working hard!";
            }
            else
            {
                fuzzyStory.text = "YOU ARE THE BLUE FISH AMONG FISHES! PRAISE THE BLUE FISH ALMIGHTY";
            }
        }
        else
        {
            WinText.text = "Tie!";
            fuzzyStory.text = "";
        }
    }
}
