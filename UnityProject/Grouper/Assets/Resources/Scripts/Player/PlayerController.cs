﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Team _team;
    [SerializeField] private float moveDistance;

    private NavMeshAgent nma;

    public Team team { get { return _team; } private set { _team = value; } }
    // Use this for initialization

    private bool mazeGenerated;

	void Start ()
    {
        nma = GetComponent<NavMeshAgent>();

        if (SceneManager.GetActiveScene().name == "GrouperMainRandom")
        {
            mazeGenerated = false;
        }
        else
        {
            mazeGenerated = true;
        }
    }

    private void OnEnable()
    {
        Maze.OnMazeFinishedGenerating += UpdateMazeGenerated;
    }

    private void OnDisable()
    {
        Maze.OnMazeFinishedGenerating -= UpdateMazeGenerated;
    }

    private void UpdateMazeGenerated()
    {
        mazeGenerated = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (mazeGenerated &&
            nma.isActiveAndEnabled && (nma.isOnNavMesh || nma.isOnOffMeshLink))
        {
            if (_team == Team.red)
            {
                nma.destination = transform.position + (Input.GetAxis("RedHorizontal") * Vector3.right + Input.GetAxis("RedVertical") * Vector3.forward).normalized * moveDistance;
            }
            else if (_team == Team.blue)
            {
                nma.destination = transform.position + (Input.GetAxis("BlueHorizontal") * Vector3.right + Input.GetAxis("BlueVertical") * Vector3.forward).normalized * moveDistance;
            }
        }
	}
}
