﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FishySpawnerController : MonoBehaviour
{
    private GameObject fishy;

    [SerializeField] private int spawnNumber;
    [SerializeField] private float innerDistance = 150f;
    [SerializeField] private float outerDistance = 300f;

    [SerializeField] private int fishiesCollectedSpawn = 3;

    private int fishiesCollected;
    private bool mazeGenerated;

    private void Awake()
    {
        fishy = Resources.Load("Prefabs/fishy") as GameObject;
    }

    private void OnEnable()
    {
        Maze.OnMazeFinishedGenerating += UpdateMazeGenerated;
        FishyController.OnGoalEnteredFishy += SpawnFishies;
        FishyController.OnPlayerCollectedFishy += SpawnOnCollectedFishy;
    }

    private void OnDisable()
    {
        Maze.OnMazeFinishedGenerating -= UpdateMazeGenerated;
       
        FishyController.OnGoalEnteredFishy -= SpawnFishies;
        FishyController.OnPlayerCollectedFishy -= SpawnOnCollectedFishy;
    }
    private void UpdateMazeGenerated()
    {
        mazeGenerated = true;
        for (short i = 0; i < spawnNumber; i++)
        {
            SpawnFishies(Team.neutral);
        }

        fishiesCollected = 0;
    }

    // Use this for initialization
    private void Start ()
    {
        if (SceneManager.GetActiveScene().name == "GrouperMain")
        {
            for (short i = 0; i < spawnNumber; i++)
            {
                SpawnFishies(Team.neutral);
            }

            fishiesCollected = 0;
        }

    }
	
    public void SpawnFishies(Team t)
    {
        float rightScale;
        float forwardScale;
        if (Random.value < 0.5f)
        {
            rightScale = Random.Range(-outerDistance, -innerDistance);

        }
        else
        {
            rightScale = Random.Range(innerDistance, outerDistance);
        }
        if (Random.value < 0.5f)
        {
            forwardScale = Random.Range(-outerDistance, -innerDistance);

        }
        else
        {
            forwardScale = Random.Range(innerDistance, outerDistance);
        }
        Instantiate(fishy, transform.position + Vector3.right * rightScale + Vector3.forward * forwardScale, Random.rotation);
    }

    private void SpawnOnCollectedFishy()
    {
        fishiesCollected++;

        if (fishiesCollected >= fishiesCollectedSpawn)
        {
            fishiesCollected = 0;
            SpawnFishies(Team.neutral);
        }
    }
}
