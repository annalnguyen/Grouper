﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class FishyController : MonoBehaviour
{
    [SerializeField] private float senseDistance;
    [SerializeField] private float goalDistance;
    public bool captureable;

    public Team team;

    private NavMeshAgent nma;
    private GameObject[] players;
    private Transform targetPlayerTransform;
    private SkinnedMeshRenderer meshRenderer;

    private bool mazeGenerated = true;

    private Material red;
    private Material blue;

    private void Awake()
    {
        team = Team.neutral;
    }

    public delegate void GoalEnteredEvent(Team t);
    public static GoalEnteredEvent OnGoalEnteredFishy;
    public delegate void CollectedByPlayerEvent();
    public static CollectedByPlayerEvent OnPlayerCollectedFishy;

	// Use this for initialization
	private void Start ()
    {
        meshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
        meshRenderer.material = Resources.Load("Mats/Fishy/Gray_Fishy") as Material;
        nma = GetComponent<NavMeshAgent>();
        players = GameObject.FindGameObjectsWithTag("Player");
        captureable = true;

        if (SceneManager.GetActiveScene().name == "GrouperMainRandom")
        {
            //mazeGenerated = false;
        }
        else
        {
            mazeGenerated = true;
        }

        red = Resources.Load("Mats/Fishy/Red_Fishy") as Material;
        blue = Resources.Load("Mats/Fishy/Blue_Fishy") as Material;
    }

    private void OnEnable()
    {
        Maze.OnMazeFinishedGenerating += UpdateMazeGenerated;
    }

    private void OnDisable()
    {
        Maze.OnMazeFinishedGenerating -= UpdateMazeGenerated;
    }

    private void UpdateMazeGenerated()
    {
        mazeGenerated = true;
    }

    // Update is called once per frame
    private void Update ()
    {
        if (mazeGenerated && captureable)
        {
            if (team == Team.neutral)
            {
                foreach (GameObject player in players)
                {
                    if (Vector3.Distance(transform.position, player.transform.position) < senseDistance)
                    {
                        //Debug.Log(Physics.Raycast(new Ray(transform.position, player.transform.position), senseDistance, LayerMask.GetMask("Walls")));
                        if (OnPlayerCollectedFishy != null)
                        {
                            OnPlayerCollectedFishy();
                        }

                        team = player.GetComponent<PlayerController>().team;
                        targetPlayerTransform = player.transform;

                        if (team == Team.red)
                        {
                            meshRenderer.material = red;
                        }
                        else if (team == Team.blue)
                        {
                            meshRenderer.material = blue;
                        }
                    }
                }
            }
            else
            {
                nma.destination = targetPlayerTransform.position;
            }
        }
	}

    public void CallGoalEnteredEvent()
    {
        OnGoalEnteredFishy(team);
        Destroy(gameObject);

    }

    public void Explode()
    {
        OnGoalEnteredFishy(Team.neutral);
        Destroy(gameObject);
    }
}
