﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{

    private Collider ExplosionTrigger;

    private FishySpawnerController Spawner;

    // Use this for initialization
    void Start()
    {
        Spawner = GameObject.Find("FishySpawner").GetComponent<FishySpawnerController>();
        ExplosionTrigger = gameObject.GetComponent<Collider>();
        StartCoroutine(DisableTrigger());
        Destroy(gameObject, 2f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other != null)
        {
            if (other.gameObject.tag == "Fishy")
            {
                if (other.gameObject != null)
                {
                    Destroy(other.gameObject);
                }

                Spawner.SpawnFishies(Team.neutral);
            }
        }
    }

    IEnumerator DisableTrigger()
    {
        yield return 0;

        ExplosionTrigger.enabled = false;
    }
}
