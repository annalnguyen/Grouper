﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingLinePosition : MonoBehaviour
{
	public Transform rodEnd;
	public Transform hookEnd;

	private LineRenderer ThisLine;

	// Use this for initialization
	private void Start ()
    {
		ThisLine = gameObject.GetComponent<LineRenderer> ();
	}
	
	// Update is called once per frame
	private void Update ()
    {
		var points = new Vector3[2];
		points [0] = rodEnd.position;
		points [1] = hookEnd.position;
		ThisLine.SetPositions (points);
	}
}
