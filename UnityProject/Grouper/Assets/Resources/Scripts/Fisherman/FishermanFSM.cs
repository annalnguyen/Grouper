﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishermanFSM : MonoBehaviour {
	public GameObject Hook;
	public GameObject Rod;
	public GameObject TNTPrefab;

	public float waitTimeTNT = 3.0f;
	public float waitTimeCast = 1.0f;
	public float remainingCooldownTNT = 0f;
	public float remainingCooldownCast = 0f;

	private Coroutine CastingCoroutine = null;
	private Transform CastingDestination;
	private Vector3 HookStart;
	public AnimationCurve castingCurve;
	public float CastingDuration = 0.3f;
	private Coroutine ReelingCoroutine = null;
	public AnimationCurve reelingCurve;
	public float ReelingDuration = 1.0f;
	public GameObject[] CastingTargets;
	private Collider HookCollider;

	public bool hyperModeActivated = false;

	public AudioSource AudSource;
	public AudioClip SoundCasting;
	public AudioClip SoundReeling;

	public FishermanStates StateCurrent = FishermanStates.Idling;

	public enum FishermanStates
	{
		Idling, // Initial and terminal
		Casting, 
		Reeling,
		Bombing
	}

    private bool active;

	// Use this for initialization
	void Start () 
	{
		AudSource = GetComponent<AudioSource>();

		CastingTargets = GameObject.FindGameObjectsWithTag("CastingTarget");

		HookStart = Hook.transform.position;
		HookCollider = Hook.GetComponent<Collider> ();
		HookCollider.enabled = false;

        active = false;
	}

    private void OnEnable()
    {
        CountdownController.OnCountdownFinished += Activate;
        TimerController.OnGameOver += Deactivate;
    }

    private void OnDisable()
    {
        CountdownController.OnCountdownFinished -= Activate;
        TimerController.OnGameOver -= Deactivate;
    }

    private void Activate()
    {
        active = true;
    }

    private void Deactivate()
    {
        active = false;
    }
	
	// Update is called once per frame
	void Update () 
	{
		if (active)
		{
			FSM_Process ();
			DecrementCooldowns ();
		}
	}

	void FSM_Process()
	{
		switch (StateCurrent)
		{
		case FishermanStates.Idling:
			if (remainingCooldownTNT == 0) { State_Idling_OnExit (); State_Bombing_OnEnter (); }
			else if (remainingCooldownCast == 0) { State_Idling_OnExit (); State_Casting_OnEnter (); }
			else { State_Idling_OnRemain (); }
			break;

		case FishermanStates.Casting:
			if (CastingCoroutine == null) { State_Casting_OnExit (); State_Reeling_OnEnter (); }
			else { State_Casting_OnRemain (); }
			break;

		case FishermanStates.Reeling:
			if (ReelingCoroutine == null) { State_Reeling_OnExit (); State_Idling_OnEnter (); }
			else { State_Reeling_OnRemain (); }
			break;

		case FishermanStates.Bombing:
			if (remainingCooldownCast == 0) { State_Bombing_OnExit (); State_Casting_OnEnter (); } 
			else { State_Bombing_OnExit (); State_Idling_OnEnter (); }
			break;
		}
	}

	void DecrementCooldowns()
	{
		float timePassed = Time.deltaTime;
		remainingCooldownTNT -= timePassed;
		remainingCooldownCast -= timePassed;

		if (remainingCooldownTNT < 0f) { remainingCooldownTNT = 0f; }
		if (remainingCooldownCast < 0f) { remainingCooldownCast = 0f; }
	}

	IEnumerator CastingCurve()
	{
		float time = 0f;

		Vector3 end = CastingDestination.position;

		while (time < CastingDuration)
		{
			time += Time.deltaTime;

			float linearT = time / CastingDuration;
			float heightT = castingCurve.Evaluate(linearT);

			float height = Mathf.Lerp(0f, 1f, heightT); // change 1 to however tall you want the arc to be

			Hook.transform.position = Vector3.Lerp(HookStart, end, linearT) + new Vector3(0f, height, 0f);

			yield return null;
		}

		// impact

		CastingCoroutine = null;
	}

	IEnumerator ReelingCurve()
	{
		float time = 0f;

		Vector3 end = HookStart;

		while (time < ReelingDuration)
		{
			time += Time.deltaTime;

			float linearT = time / ReelingDuration;
			float heightT = reelingCurve.Evaluate(linearT);

			float height = Mathf.Lerp(0f, 0f, heightT); // change 0 to however tall you want the arc to be

			Hook.transform.position = Vector3.Lerp(CastingDestination.position, end, linearT) + new Vector3(0f, height, 0f);

			yield return null;
		}

		// impact

		ReelingCoroutine = null;
	}



	#region Enter, Remain, and Exit Functions
	
	void State_Idling_OnEnter()
	{
		StateCurrent = FishermanStates.Idling;
	}

	void State_Idling_OnRemain()
	{

	}

	void State_Idling_OnExit()
	{

	}
	// // // // // // // // // // // // // //
	void State_Casting_OnEnter()
	{
		StateCurrent = FishermanStates.Casting;

		AudSource.clip = SoundCasting;
		AudSource.Play ();

		CastingDestination = CastingTargets[Random.Range (0, CastingTargets.Length - 1)].transform;
		var pos = CastingDestination.position;
		pos.y = transform.position.y;
		transform.LookAt(pos);
		HookStart = Hook.transform.position;

		CastingCoroutine = StartCoroutine(CastingCurve ());
	}

	void State_Casting_OnRemain()
	{

	}

	void State_Casting_OnExit()
	{
		AudSource.Stop ();
	}
	// // // // // // // // // // // // // //
	void State_Reeling_OnEnter()
	{
		StateCurrent = FishermanStates.Reeling;

		AudSource.clip = SoundReeling;
		AudSource.Play ();

		HookCollider.enabled = true;
		ReelingCoroutine = StartCoroutine(ReelingCurve ());
	}

	void State_Reeling_OnRemain()
	{

	}

	void State_Reeling_OnExit()
	{
		AudSource.Stop ();

		remainingCooldownCast = waitTimeCast;
        HookCollider.GetComponent<HookBehavior>().LetGo();
		HookCollider.enabled = false;
	}
	// // // // // // // // // // // // // //
	void State_Bombing_OnEnter()
	{
		StateCurrent = FishermanStates.Bombing;

		Quaternion rot = Quaternion.FromToRotation(Vector3.up, gameObject.transform.up);
		Vector3 pos = gameObject.transform.position;
		Instantiate(TNTPrefab, pos, rot);
	}

	void State_Bombing_OnRemain()
	{

	}

	void State_Bombing_OnExit()
	{
		remainingCooldownTNT = waitTimeTNT;
	}

	#endregion
}
