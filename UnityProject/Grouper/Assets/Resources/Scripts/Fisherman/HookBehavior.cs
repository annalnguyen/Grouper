﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HookBehavior : MonoBehaviour
{
    private List<Transform> capturedFishies = new List<Transform>();

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Fishy")
        {
            other.gameObject.GetComponent<NavMeshAgent>().enabled = false;
            other.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material = Resources.Load("Mats/Fishy/Gray_Fishy") as Material;
            other.gameObject.GetComponent<FishyController>().team = Team.neutral;
            other.gameObject.GetComponent<FishyController>().captureable = false;
            other.gameObject.transform.SetParent(transform);
            capturedFishies.Add(other.gameObject.transform);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Fishy")
        {
            other.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material = Resources.Load("Mats/Fishy/Gray_Fishy") as Material;
            other.gameObject.GetComponent<FishyController>().team = Team.neutral;
            other.gameObject.GetComponent<FishyController>().captureable = false;
        }
    }

    public void LetGo()
    {
        capturedFishies.ForEach(Unparent);
        capturedFishies.Clear();
    }

    private static void Unparent(Transform t)
    {
        if (t != null)
        {
            t.gameObject.GetComponent<NavMeshAgent>().enabled = true;
            t.gameObject.GetComponent<FishyController>().captureable = true;
            t.parent = null;
        }
    }
}
