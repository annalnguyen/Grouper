﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TNTBehavior : MonoBehaviour {
	public GameObject ExplosionPrefab;

	private Transform TNTDestination;
	private Coroutine BombingCoroutine = null;
	public AnimationCurve bombingCurve;
	public float BombingDuration = 0.3f;
	public GameObject[] TNTTargets;

	private Vector3 StartingPosition;

	void Start()
	{
		TNTTargets = GameObject.FindGameObjectsWithTag("TNTTarget");

		StartingPosition = gameObject.transform.position;

		TNTDestination = TNTTargets[Random.Range (0, TNTTargets.Length - 1)].transform;
		BombingCoroutine = StartCoroutine(BombingCurve ());
	}

	void BeginCountdown ()
	{
		Invoke ("Countdown", 2f);
	}

	void Countdown()
	{
		Quaternion rot = Quaternion.FromToRotation(Vector3.up, gameObject.transform.up);
		Vector3 pos = gameObject.transform.position;
		Instantiate(ExplosionPrefab, pos, rot);
		Destroy(gameObject);
	}

	IEnumerator BombingCurve()
	{
		float time = 0f;

		Vector3 end = TNTDestination.position;

		while (time < BombingDuration)
		{
			time += Time.deltaTime;

			float linearT = time / BombingDuration;
			float heightT = bombingCurve.Evaluate (linearT);

			float height = Mathf.Lerp (0f, 3f, heightT); // change 3 to however tall you want the arc to be

			gameObject.transform.position = Vector3.Lerp (StartingPosition, end, linearT) + new Vector3 (0f, height, 0f);

			yield return null;
		}
		// impact

		BombingCoroutine = null;
		BeginCountdown ();
	}
}
