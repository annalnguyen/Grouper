# Grouper

## Unity Version
Unity 2017.2.0f3.

This version can be found on
[Unity's Download Archive](https://unity3d.com/get-unity/download/archive).
The correct version is Unity 2017.2.0.

## Platform
Windows 10 x64

## Build Instructions
The three included scenes should be:

0.  _Scenes/StarMenu
1.  _Scenes/GrouperMain
2.  _Scenes/GrouperMainRandomNewNavMesh

#### Player Settings
The game was designed for a 900x900 resolution but should be able to letterbox
to most reasonable aspects.
